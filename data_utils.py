import os
import re
import cPickle as pickle

PAD_ID = 6
GO_ID = 0
EOS_ID = 1

def Word2vec():
  with open('data_set/word2vec_couplet.txt','rb') as f:
    
    wordMisc = pickle.load(f)
    word2id = wordMisc['word2id']
    id2word = wordMisc['id2word']
    P_Emb = wordMisc['P_Emb']
    P_sig = wordMisc['P_sig']
    
  return word2id,id2word,P_Emb,P_sig
