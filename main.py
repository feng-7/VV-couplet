#coding:utf-8

import math
import os
import random
import sys
import time

import cPickle as pickle

import numpy as np

np.set_printoptions(threshold='nan')

# from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf

import data_utils
import seq2seq_model


word2id,id2word,P_Emb,P_sig = data_utils.Word2vec()

#word2id['END1'] =4
# word2id['pad']=4778
# id2word[4778]='pad'
# P_Emb.append([0.0]*200)

tf.app.flags.DEFINE_float("learning_rate", 0.1, "Learning rate.")
tf.app.flags.DEFINE_float("learning_rate_decay_factor", 0.99,
                          "Learning rate decays by this much.")
tf.app.flags.DEFINE_float("max_gradient_norm", 5.0,
                          "Clip gradients to this norm.")
tf.app.flags.DEFINE_integer("batch_size", 1,
                            "Batch size to use during training.")
tf.app.flags.DEFINE_integer("size", 500, "Size of each model layer.")
tf.app.flags.DEFINE_integer("num_layers", 1, "Number of layers in the model.")
tf.app.flags.DEFINE_integer("vocab_size", len(P_Emb), "vocabulary size.")
tf.app.flags.DEFINE_string("data_dir", os.path.join(os.path.split(os.path.dirname(__file__))[0],os.path.split(os.path.dirname(__file__))[1],
                            'data_set'), "Data directory")
tf.app.flags.DEFINE_string("train_dir", os.path.join(os.path.split(os.path.dirname(__file__))[0],os.path.split(os.path.dirname(__file__))[1],
                            'model'), "Training directory.")
tf.app.flags.DEFINE_string("model_name",'', "Model name.")
tf.app.flags.DEFINE_integer("max_train_data_size", 0,
                            "Limit on the size of training data (0: no limit).")
tf.app.flags.DEFINE_integer("steps_per_checkpoint",9600,
                            "How many training steps to do per checkpoint.")
tf.app.flags.DEFINE_boolean("decode", False,
                            "Set to True for interactive decoding.")
tf.app.flags.DEFINE_boolean("self_test", False,
                            "Run a self-test if this is set to True.")
FLAGS = tf.app.flags.FLAGS

# We use a number of buckets and pad to the closest one for efficiency.
# See seq2seq_model.Seq2SeqModel for details of how they work.
_buckets = [(33,33)]

entity_arr =[] 

with open('data_set/names.txt','r') as source_file:
  lines = source_file.readlines()

  for line in lines:

    words = line.strip().split(' ')

    entity_arr.append(words)

def search_entity(input_str):

  entity_info = []
  input_str = input_str.strip().replace(' ','').decode('utf-8')

  for words_arr in entity_arr:

    for word in words_arr:            

      word = word.decode('utf-8')

      if word in input_str or word == input_str:

	selected_words = []

	for temp_w in words_arr:

	  temp_w = temp_w.decode('utf-8')

	  if (temp_w != word) and (len(temp_w) == len(word)):

	    selected_words.append(temp_w)

	selected_word = random.choice(selected_words)

	ind = input_str.index(word)

	entity_info.append([ind, len(selected_word), word.encode('utf-8'), selected_word.encode('utf-8')])

  entity_info.sort(key=lambda elem: elem[0])

  for info in entity_info:

    ind = input_str.index(info[-2].decode('utf-8'))

    input_str = input_str[:ind] + input_str[(ind + len(info[-2].decode('utf-8'))):]

  entity_info.append(''.join([w.encode('utf-8') for w in input_str]))

  return entity_info

def read_data(source_path):

  #temp_file = open('poem7_ uniformlength.txt','a')
   
  data_set = [[] for _ in _buckets]
  with tf.gfile.GFile(source_path, mode="r") as source_file:
      # source, target = source_file.readline().split('==')
      counter = 0
      charac_num = len(word2id)
      for line in source_file.readlines():
        source,target = line.split('==')
        counter += 1
        if counter % 100000 == 0:
          print("  reading data line %d" % counter)
          sys.stdout.flush()
        source_words = []
        target_words = []
	
	source_words += ('START1 ' + source + ' END1').split(' ')

        target_words += target.strip().split(' ') 
        source_ids = [word2id.get(x, charac_num-1) for x in source_words]
        target_ids = [word2id.get(x, charac_num-1) for x in target_words]
        target_ids.append(data_utils.EOS_ID)

        
        #if len(target_ids) == 40 and len(source_ids) == 16:
            #temp_file.write(line)
        
        for bucket_id, (source_size, target_size) in enumerate(_buckets):
          if len(source_ids) <= source_size and len(target_ids) <= target_size:
	    tone_set = [0.0,0.0,0.0]
            words_sum = 0
	    
	    if ( len(source.split(' ')) != len(target.strip().split(' '))):
	      break
	    
	    yun_ind_set = []
            
            for pos_i in range(len(target_words) + 2):
	      temp_pos_mask = None
	      
	      temp_pos = [0.0]*(_buckets[0][1]) 
	      #print (target)
	      temp_pos[pos_i] = 1.0

              yun_ind_set.append(temp_pos)
            
            data_set[bucket_id].append([[source_ids, target_ids],yun_ind_set])
	    
  return data_set

global yunmuModel

yunmuModel = None

def create_model(session, is_predict):
  """Create translation model and initialize or load parameters in session."""
  model = seq2seq_model.Seq2SeqModel(
      FLAGS.vocab_size, FLAGS.vocab_size, _buckets,
      FLAGS.size, FLAGS.num_layers, FLAGS.max_gradient_norm, FLAGS.batch_size,
      FLAGS.learning_rate, FLAGS.learning_rate_decay_factor,
      is_predict=is_predict,cell_initializer=tf.constant_initializer(np.array(P_Emb,dtype=np.float32)))
  ckpt = tf.train.get_checkpoint_state(FLAGS.train_dir)
  if ckpt and tf.gfile.Exists(ckpt.model_checkpoint_path):
    print("Reading model parameters from %s" % ckpt.model_checkpoint_path)
    session.run(tf.initialize_all_variables())
    model.saver.restore(session, ckpt.model_checkpoint_path)
  else:
    print("Created model with fresh parameters.")
    session.run(tf.initialize_all_variables())
  return model



def train():
  """Train a en->fr translation model using WMT data."""
  # Prepare WMT data.
  # print("Preparing WMT data in %s" % FLAGS.data_dir)
  # en_train, fr_train, en_dev, fr_dev, _, _ = data_utils.prepare_wmt_data(
  #     FLAGS.data_dir, FLAGS.en_vocab_size, FLAGS.fr_vocab_size)
  # train_set = read_data('poem5_9k_theme.txt', '')
  
  with tf.Session() as sess:
    # Create model.

    print("Creating %d layers of %d units." % (FLAGS.num_layers, FLAGS.size))
    model = create_model(sess, False)
    # Read data into buckets and compute their sizes.
    print ("Reading development and training data (limit: %d)."
           % FLAGS.max_train_data_size)

    sys.stdout.flush()

    # dev_set = read_data('test_poem5_9k_theme.txt', '')
    train_set = read_data('data_set/corpus_train_set.txt')
    train_bucket_sizes = [len(train_set[b]) for b in xrange(len(_buckets))]
    train_total_size = float(sum(train_bucket_sizes))

    # A bucket scale is a list of increasing numbers from 0 to 1 that we'll use
    # to select a bucket. Length of [scale[i], scale[i+1]] is proportional to
    # the size if i-th training bucket, as used later.
    train_buckets_scale = [sum(train_bucket_sizes[:i + 1]) / train_total_size
                           for i in xrange(len(train_bucket_sizes))]

    # This is the training loop.
    step_time, loss = 0.0, 0.0
    current_step = 0
    previous_losses = []

    
    while True:
      # Choose a bucket according to data distribution. We pick a random number
      # in [0, 1] and use the corresponding interval in train_buckets_scale.
      # random_nu``mber_01 = 0.5
      random_number_01 = np.random.random_sample()
      bucket_id = min([i for i in xrange(len(train_buckets_scale))
                       if train_buckets_scale[i] > random_number_01])


      # Get a batch and make a step.
      start_time = time.time()
      if(model.global_step.eval()%FLAGS.steps_per_checkpoint == 0):
          for i_th in range(len(train_buckets_scale)):
              random.shuffle(train_set[0])

      batch_start_id = current_step%FLAGS.steps_per_checkpoint

      encoder_inputs,reverse_encoder_inputs, decoder_inputs, target_weights,sequence_length,batch_encoder_weights,type_list,yun_pos_inputs = \
          model.get_batch(train_set, bucket_id,batch_start_id,P_sig)
      
      step_loss, _ , _ = model.step(sess, encoder_inputs,reverse_encoder_inputs, decoder_inputs,
                                   batch_encoder_weights,target_weights,sequence_length,
                                   bucket_id, 1.0,False,False,yun_pos_inputs,sig_weight=type_list)
      
      step_time += (time.time() - start_time)
      loss += step_loss / FLAGS.steps_per_checkpoint
      current_step += 1
      
      print (current_step)
      print ("%.3f"%step_loss)
      
      #print (step_loss_1)
      #print (step_loss_2)
      #print (str(current_step) + 'th') 
      #print (embed_inputs)
      #print (batch_encoder_weights)      
      sys.stdout.flush()
      # Once in a while, we save checkpoint, print statistics, and run evals.
      if current_step % (FLAGS.steps_per_checkpoint) == 0:
        # Print statistics for the previous epoch.
        # perplexity = math.exp(loss) if loss < 300 else float('inf')
	
        print ("global step %d  step-time %.2f loss "
               "%.2f" % (current_step/FLAGS.steps_per_checkpoint,
                         step_time, loss))
	
        # Decrease learning rate if no improvement was seen over last 3 times.
        previous_losses.append(loss)
        checkpoint_path = os.path.join(FLAGS.train_dir,str(int(current_step/FLAGS.steps_per_checkpoint) + 13 ) + "th_model.ckpt+cost=" + str(loss))
        model.saver.save(sess, checkpoint_path)
        step_time, loss = 0.0, 0.0

        sys.stdout.flush()


def decode():
   
  with tf.Session() as sess:
    
    bucket_id = 0    
    is_feed = True
    type_sig = 49
    charac_num = len(word2id)
    pre_words_e = []
    # Create model and load parameters.
    print ("build the model")
    
    model = create_model(sess, True)
    path = os.getcwd() + '/model'
    
    #loaded_model_name = '42th_model.ckpt+cost=33.1762374783'
    loaded_model_name = FLAGS.model_name
    print("Reading model parameters from %s" % loaded_model_name)
    sess.run(tf.initialize_all_variables())
    
    model.saver.restore(sess, path + '/' +  loaded_model_name)    
    print ('input the first sentence:')
    sentence = sys.stdin.readline()
    while sentence:
      # Get token-ids for the input sentence.
      
      #sentence = '杨柳岸晓风残月'
      input_info = search_entity(sentence.strip())
      
      sen_w = ['START1'] + [w for w in input_info[-1].decode('utf-8')] + ['END1']
      in_sentences_ids = [word2id.get(w.encode('utf-8'), charac_num-1) for w in sen_w]
      
      x_in = []
      for q in in_sentences_ids:
	x_in.append(q)
      
      #first_w = 'START'
      #pre_words_e.append(word2id.get(first_w, charac_num-1))
      
      yun_ind_set = []
    
      for pos_i in range(len(in_sentences_ids)):
	temp_pos_mask = None
    
	temp_pos = [0.0]*(_buckets[0][1]) 
	#print (target)
	temp_pos[pos_i] = 1.0
    
	yun_ind_set.append(temp_pos)
    
      # Get a 1-element batch to feed the sentence to the model.
      encoder_inputs,reverse_encoder_inputs, decoder_inputs, target_weights,sequence_length,batch_encoder_weights,sig_list,yun_pos_inputs = model.get_batch(
	{bucket_id: [[(x_in, pre_words_e),yun_ind_set]]}, bucket_id, 0,P_sig)
      # Get output logits for the sentence.
      attens, state_outputs,loss, output_logits,atten_inputs,hiddens,embed_inputs ,state_fw,state_bw,reverse_embed_encoder_inputs,tmp = model.step(sess, encoder_inputs,reverse_encoder_inputs, decoder_inputs,
	                                                                                                                                           batch_encoder_weights,target_weights,sequence_length, bucket_id,
	                                                                                                                                           1.0,True,False,yun_pos_inputs,sig_weight=np.array([[0.0]*len(P_sig[type_sig])]))    
    
      #output_arr = [id2word[e[0].argsort(axis=0)[-1]] for e in output_logits]
      
      #print (' '.join(output_arr[:(len(in_sentences_ids) - 2)]))
      if len(input_info) == 1:
	print (''.join([id2word[int(w)] for w in output_logits[:(len(in_sentences_ids) - 2)]]))      
      else:
	temp_arr = [id2word[int(w)] for w in output_logits[:(len(in_sentences_ids) - 2)]]
	
	for item in input_info[:-1]:
	  temp_arr = temp_arr[:item[0]] + [ w.encode('utf-8') for w in item[-1].decode('utf-8')] + temp_arr[item[0]:]
	  
	print (''.join(temp_arr))
	  
      print ('input the first sentence:')
      sentence = sys.stdin.readline()
def main(_):
  if FLAGS.decode:
    decode()
  else:
    train()


if __name__ == "__main__":
  tf.app.run()
