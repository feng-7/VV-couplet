# VV_Couplet

## Introduction

VV_Couplet is the core code of our couplet system.

## User Manual

### Installation

#### System Requirements

* Linux or MacOS
* Python 2.7

We recommand to use GPUs:

* NVIDIA GPUs 
* cuda 7.5

#### Installing Prerequisites

##### CUDA 7.5 environment
Assume CUDA 7.5 has been installed in "/usr/local/cuda-7.5/", then environment variables need to be set:

```
export PATH=/usr/local/cuda-7.5/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/cuda-7.5/lib64:$LD_LIBRARY_PATH 
```
##### Tensorflow 0.10
To have tensorflow 0.10 installed, serval methods can be applied. Here, we only introduce the installation through virtualenv. And we install the tensorflow-gpu, if you choose to use CPU, please install tensorflow of cpu.

```
pip install virtualenv --user
virtualenv --system-site-packages tf0.10  
source tf0.10/bin/activate
download tensorflow_0.10 from https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow-0.10.0-cp27-none-linux_x86_64.whl
pip install tensorflow_0.10
```
##### Test installation
Get into python console, and import tensorflow. If no error is encountered, the installation is successful.

```
Python 2.7.5 (default, Nov  6 2016, 00:28:07) 
[GCC 4.8.5 20150623 (Red Hat 4.8.5-11)] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import tensorflow 
I tensorflow/stream_executor/dso_loader.cc:108] successfully opened CUDA library libcublas.so locally
I tensorflow/stream_executor/dso_loader.cc:108] successfully opened CUDA library libcudnn.so locally
I tensorflow/stream_executor/dso_loader.cc:108] successfully opened CUDA library libcufft.so locally
I tensorflow/stream_executor/dso_loader.cc:108] successfully opened CUDA library libcuda.so.1 locally
I tensorflow/stream_executor/dso_loader.cc:108] successfully opened CUDA library libcurand.so locally
>>> 
```

### Quick Start
If you are from cslt, you can read run_in_cslt.txt and run the program on servers of cslt.

If you are not from cslt, make sure you have finished the installations above, then follow the commands below.

Prepare:
```
git clone git@gitlab.com:feng-7/VV-couplet.git
VV-couplet
```

Train a model:
```
python main.py --decode False --batch_size 80

the models which are generated when the code runs through a round will be Saved in the dir 'model'.

```

run a selected model:
```
python main.py --decode True --batch_size 1 --model_name "input the name of the selected model" 

the model is selected in the dir 'model'.
```

## License
Open source licensing is under the Apache License 2.0, which allows free use for research purposes. 
For commercial licensing, please email byryuer@gmail.com.

## Development Team

Project leaders: Dong Wang

Project members: Jiyuan Zhang, Shiyue Zhang, Zheling Zhang

## Contact

If you have questions, suggestions and bug reports, please email [zhang_ml@pku.edu.cn].
